/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.event.chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Predaco
 */
public class Handler implements Runnable{
    
    private Socket p_client;
    
    public Handler(Socket client)
    {
        this.p_client = client;
    }
    
    @Override
    public void run() {
            
            OutputStream out = null;
        try {
            //Streams
            out = p_client.getOutputStream();
            PrintWriter writer = new PrintWriter(out);      //Kann nur für strings genutzt werden.
            InputStream in = p_client.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            //------------------------------------------------------------------
            
            String s = null;
            while((s = reader.readLine()) != null)
            {
                writer.write(s + "\n");
                writer.flush();         // Server Echo
                System.out.println("Empfangen vom Client: " + s);
            }
            writer.close();
            reader.close();
        } catch (IOException ex) {
            Logger.getLogger(Handler.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                out.close();
            } catch (IOException ex) {
                Logger.getLogger(Handler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
