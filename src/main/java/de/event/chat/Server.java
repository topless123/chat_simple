/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.event.chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
//import java.net.SocketException;

/**
 * @author Topless Vagina
 */
public class Server {
    
    public static void main(String[] args) {
        
        ServerSocket server;
        ExecutorService executor = Executors.newFixedThreadPool(30);
        
        try {
            server = new ServerSocket(5555);
            System.out.println("Server gestartet");            
            
            while(true)
            {
                try {
                    Socket client = server.accept();
                    
                    //Thread t = new Thread(new Handler(client));
                    //t.start();
                    executor.execute(new Handler(client));
                    
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }	
}
