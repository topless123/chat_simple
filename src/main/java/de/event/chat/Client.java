/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.event.chat;

import java.io.BufferedReader;
import java.net.Socket;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Topless Vagina
 */
public class Client implements Runnable{
    
    private static int i_a;
    
    public static void main(String[] args) {
        
        i_a = 1;
        
        new Thread(new Client()).start();
        new Thread(new Client()).start();
        new Thread(new Client()).start();
        new Thread(new Client()).start();
    }

    
    @Override
    public void run() {
        try {
            Socket client = new Socket("localhost", 5555);
            System.out.println("Client gestartet");
            
            //Streams
            
            OutputStream out = client.getOutputStream();
            PrintWriter writer = new PrintWriter(out); //Kann nur für strings genutzt werden.
            try {
                InputStream in = client.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                
                //------------------------------------------------------------------
                
                writer.write(i_a + " Test\n");
                writer.flush();
                i_a++;
                
                String s = null;
                
                while((s = reader.readLine()) != null) {
                    System.out.println("Empfangen vom Echo: " + s);
                }
                
                reader.close();
            } finally {
            //reader.close();
            writer.close();
        }
            
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
        } catch (SocketException ex) {
            System.out.println("Connection Lost\n");
            System.out.println(ex);
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            //reader.close();
            //writer.close();
        }
    }
    
}
